FROM node:10-alpine
WORKDIR /usr/src/app

# Requirements of bcrypt
RUN apk update
RUN apk --no-cache add --virtual builds-deps build-base python git

#RUN npm set config registry http://172.42.42.42:4873

#Install PM2 Global
RUN npm install -g pm2

# Copy package.json
COPY ./package.* .

# Install dep
RUN npm install -i

CMD ["pm2-runtime", "config/dev.config.js", "--watch"]
