var assert = require('assert');

const db = require("../src/db/db.js");
const UserModel = require('../src/models/User.js');

const User = require("../src/actions/user.js");

const CollectionUsers = "users";

const user01 = "user01"
const publicKeyUser01 = "-----BEGIN PGP PUBLIC KEY BLOCK-----\nVersion: BCPG v@RELEASE_NAME@\n\nmQENBF0MygsDCAC05yQjeGKhabcj06lReXvCB9DLbfXUo5TRJTNQ0yQsFX0NzS4h\nMqCkVKVWcPG2YjafMthNa75FvA1j2ecGtk+30v1rQzBcZbnG8NA7mDE924AK3FDq\numvZAq3RCVGI0ZKInj0K64Yn3GiqW5nNbgX3DdIRORadhhDgGmmHpHPbgtV7xA3h\nT4AYhXnxIKvo2JdqtM6+otpM5HebZ3MfSOL+v57WA7jVUaKdnZ/wEgQg7r97lbze\nhsUaPDOLgQTUDkn3Xe4J4yf7073Jq2dm0JXtSPSgy+M+NxCkWz0NMDNLCrbtmUDW\nBxr09CbNyVI77YV8nrYgIwHR3u5tk4w4EQKxABEBAAG0E2phbi5yYWJlQGtpYm90\ndS5uZXSJAS4EEwMCABgFAl0MygwCG4MECwkIBwYVCAIJCgsCHgEACgkQp/Rb/s2n\n441Y/Qf/XIfDPwUuNP/fdKuIC0mS7tnQ7mT0+8UEHgBeF231WBkdi5AiCzaHJQ4l\nkqPGWnVoNAgqduqbiMwSy43TVUnKOafUXJsIJQtTsqkc0kUuEpo1Xkcexzo6g54G\nLyD5eV42zQx7wK3QJbk6D1uFSMaqTgPDcXBB+J0O0H8WQJ1ycwzHyRLu1J3gjONW\nuoSFVaPGMKLmO/RisoDCv9L33zNpeSlJ4wPsx1BouTk2P2YEm3gX1wMJMbVRojWY\n0JKU0QQ3u5buxGqv5AYC3VNflZ7j4AGKkQOveTkv7Grh+0GxYTW7MiaOC26JMNrC\njCu6rPZkgWQ3Wauia6VBqXmIVeuSNLkBDQRdDMoMAggA6gCvXly/47J9nEezPebj\nRsvnL2pKRT3PHOyXfm+8YVaGl4/WRa8njGyev+QbzDd+L1biwCEEORUixTA74l0j\nnXdxYwjyckh/wKy/ALfDnVBFQZbTJRwgM4SfXDBl4q4wMZWHG/cBtBCNsR9PwdR7\nf5ggquAP89JBYPRliChk4S+sVkpEb5N98DcYTkgYYf4aL8cC7aGkrNaNc5aSX/vK\nM7gGM+WkETpO/sR/bi451NJJ29YmvAe7lwO/ExmeT6/qveDkmzJNUYnCDOsQzcEY\nK4VFqxh6wJKicEDDpeM9WQ4ym5k0XM56hsA+VvyiQzrBvD7qCcbvuPkwRnuSf84Z\nUQARAQABiQEfBBgDAgAJBQJdDMoMAhsMAAoJEKf0W/7Np+ON7S8H/jh3SGZygR/N\ntHQ2i640/aHtwi5XmfeFJkT5ExpeBzhPjWF/CBbM96gx4Rb2Q7A5c+8AsXKCg7bp\nFPgycSM1ozE7PSpnievV2qiS9IvhEn+MThXlUrMqG1E6PiRXqRY4Yn1jmMHsgQv+\nqQVkOwcGK6AA7qn/IMANyxlhl2beh4YDyJSuz2mSXWnlGOMGTcLxy2E0mfw3R2Iq\ncHfZR8I9KtqZdh6FfIBarqaH2sNOYUhJI5v5z6Wube4TMkxNEEyxgPyAVt19ZAGq\nQj6XrjH4ASv1t5dnFCW1tEvqxjLJoFezqMtMB54E0gAYLuGH6CKeCOy5n9I4ObH7\nzRUSfv1nKKo=\n=Q3d9\n-----END PGP PUBLIC KEY BLOCK-----\n";

var username01 = "";

beforeEach(function() {
    // DELETE DB
});

// Create User
describe('User Creation', function() {
    it('Create User', function() {
	return new Promise(function(resolve) {
	    User.create(user01, publicKeyUser01).then(function(value){
		assert.equal(value.message, 'User created');
		username01 = value.username;
		resolve();
	    });
	});
    });
    it('Create same User', function() {
	return new Promise(function(resolve, reject) {
	    //User.create(user01, publicKeyUser01).then(function(lol){
	    User.create(user01, publicKeyUser01).then(function(value){
		console.log(Date.now() + " " + "not good");
		reject();
	    }).catch(function(err){		    
		assert.equal(err.message, 'publicKey ' + publicKeyUser01 + ' already existing');	
		resolve();
	    });
	    //});
	});
    });
    it('Create User with Wrong publicKey (not PGP compatible)', function() {
	return new Promise(function(resolve) {
	    User.create(user01, "lol").then(function(value){
	    }).catch(function(err){		    
		assert.equal(err.message, "Error: Error encrypting message: No keys, passwords, or session key provided.");	
		resolve();
	    });
	});
    });
});

// GetPublickey User01
// getPublicKey valid
describe('getPublicKey', function() {
    it('getPublicKey User01', function() {
	return new Promise(function(resolve) {
	    User.getPublicKey(username01).then(function(value){
		assert.equal(value, publicKeyUser01);
		resolve();
	    });
	});
    });
    it('getPublicKey fakeuser', function() {
	return new Promise(function(resolve) {
	    User.getPublicKey("fakeuser").then(function(value){
		//NOTHING HERE
	    }).catch(function(err){
		assert.equal(err.message, "Username fakeuser not existing");
		resolve();
	    });
	});
    });
});

/*
// GetPublickey User01
// getPublicKey non valid
describe('getPublicKey fakeuser', function() {
    
});
*/
