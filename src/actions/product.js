// Import Node Modules
const jwt = require("jsonwebtoken");

const User = require("./user.js");

// Import User Model
const UserModel = require("../models/User.js");
const ProductModel = require("../models/Product.js");

const cache = require("../db/cache.js").client;

const { promisify } = require("util");

const getAsync = promisify(cache.get).bind(cache);

const { ObjectID } = require("mongodb");

const ipfsClient = require('ipfs-http-client');
const ipfs = ipfsClient('ipfs.infura.io', '5001', {protocol: 'https'})

const Product = {
    async create(title, content, createdBy) {
	return new Promise(function(resolve, reject) {
	    const product = new ProductModel({
		title,
		content,
		createdBy,
		updatedBy: createdBy
	    });
	    product
		.save()
		.then(() => {
		    cache.set(product._id.toString(), product.toString());
		    resolve({
			message: "PRODUCT_ADDED",
			productID: product._id
		    });
		})
		.catch(function(err) {
		    reject(new Error(err));
		});
	});
    },
    uploadPicture(productID, picture, userID) {
	return new Promise(function(resolve, reject) {	
	    ipfs.add({"content": picture}, function (err, file) {
		if (err) {
                    console.log(Date.now() + " " + err);
		    reject(err);
		}
		console.log(Date.now() + " " + file)
		ProductModel.updateMany(
                    { _id: productID },
                    { $set: { picture: file[0].hash } },
                    function(error, comments) {
                        if (comments !== undefined){
                            resolve({
                                message: "PICTURE_UPLOADED",
                                productID: productID
                            });
                            ProductModel.find({ _id: productID }, function(error, products) {
                                if (products[0] !== undefined) {
                                    cache.set(products[0].id.toString(), products[0].toString());
                                }
                            });
                        }
		    });
	    });
	});
    },
    delete(productID, userID) {
      return new Promise(function(resolve, reject) {
	  ProductModel.find({ _id: productID }, function(error, comments) {
              if (comments[0] === null || comments[0] === undefined)
		  reject(new Error("PRODUCT_NOT_FOUND"));
              else {
		  console.log(Date.now() + " " + comments[0]);
		  if (comments[0].createdBy.toString() === userID.toString()){
		      cache.del(productID.toString());
		      ProductModel.deleteOne({ _id: productID }, function(error, comments) {
			  console.log(Date.now() + " " + `${JSON.stringify(comments, null, 4)}`);
			  resolve("PRODUCT_DELETED");
		      });
		  }
		  else{
		      reject("PRODUCT_NOT_OWNED");
		  }
              }
	  });
      }).catch(function(err) {
	  throw new Error(err);
      });
  },
  isProductExist(id) {
    return new Promise(function(resolve, reject) {
	cache.get(id.toString(), (err, result) => {
        if (result !== undefined && result !== null) {
          resolve(true);
        } else {
          ProductModel.find({ _id: id }, function(error, comments) {
            if (comments !== undefined) {
              // cache.set(id, comments[0].toString());
              resolve(true);
            } else {
              resolve(false);
            }
          });
        }
      });
    });
  },
  getProductsFromUserID(id) {
    return new Promise(function(resolve, reject) {
      ProductModel.find(
          { createdBy: id },{"__v": 0},
        function(error, products) {
            resolve(products);
        }
      );
    });
  },
    get(){
	return new Promise(function(resolve, reject){
	    ProductModel.find({}, {"content": 0, "__v": 0}, function(error, comments){
		if (comments[0] !== undefined)
		    resolve(comments);
		else
		    reject("NO_PRODUCTS");
	    })
	});
    },
    getProjectFromID(projectID){
	return new Promise(function(resolve, reject){
	    /*cache.get(projectID, (err, result) => {
		if (result !== undefined && result !== null) {
		    console.log(Date.now() + " " + result);
		    JSON.parse(JSON.parse(result));
		    resolve(result);
		}
		else{*/
		    ProductModel.find({ _id: projectID },{"__v": 0}, function(error, comments){
			if (comments.length === 0 || undefined)
			    reject("NO_PRODUCTS");
			else{
			    resolve(comments[0]);
			    cache.set(comments[0]._id.toString(), JSON.stringify(comments[0]));
			}
		    });
		//}
	    //});
	});
    },
    edit(id, title, content, updatedBy) {
	return new Promise(function(resolve, reject) {
	ProductModel.find({ _id: id }, function(error, comments) {
            if (comments !== undefined) {
		if (comments[0].createdBy.toString() === updatedBy.toString()){
		    title = title || comments[0].title;
		    content = content || comments[0].content;
		    ProductModel.updateMany(
			{ _id: id },
			{ $set: { title, content, updatedBy, updated: new Date(Date.now()).toISOString()} },
			function(error, comments) {
			    if (comments !== undefined){
				resolve({
				    message: "PRODUCT_EDITED",
				    productID: id
				});
				ProductModel.find({ _id: id }, function(error, products) {
				    if (products !== undefined) {
					cache.set(products[0].id.toString(), products[0].toString());
				    }
				});
			    }
			    else reject("ERROR");
			}
		    );
		}
		else{
		    reject("PRODUCT_NOT_OWNED");
		}
	    }
	});
    }).catch(function(err) {
          throw new Error(err);
      });
 }
};

module.exports = Product;
