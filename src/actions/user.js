// Import Node Modules
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

// Import User Model
const UserModel = require("../models/User.js");

const cache = require("../db/cache.js").client;

const ipfsClient = require('ipfs-http-client');
const ipfs = ipfsClient('ipfs.infura.io', '5001', {protocol: 'https'})


var User = {
    create(username, password) {
	return new Promise(function(resolve, reject) {
	    UserModel.find({ username }, function(error, comments) {
		if (comments[0] !== undefined) {
		    reject(new Error("USER_FOUND"));
		} else {
		    bcrypt.hash(password, 10, function(err, hash) {
			const user = new UserModel({ username, password: hash });
			cache.set(user._id, user);
			resolve({
			    message: "USER_CREATED",
			    username
			});
			user.save().then(() => {
			    console.log(Date.now() + " " + "user save in DB");
			});
		    });
		}
	    });
	}).catch(function(err) {
	    throw new Error(err.message);
	});
    },
    editUsername(userID, newUsername){
        return new Promise(function(resolve, reject) {
	    UserModel.find({ username: newUsername }, function(error, comments){
		if (comments[0] !== undefined)
		    reject(new Error("USERNAME_ALREADY_TAKEN"));
		else{
		    UserModel.find({ _id: userID }, function(error, comments) {
			if (comments !== undefined) {
			    var username = "";
			    username = newUsername || comments[0].username;
			    console.log(username);
			    UserModel.updateMany(
				{ _id: userID },
                            { $set: { username, updated: new Date(Date.now()).toISOString()} },
				function(error, comments) {
				    if (comments !== undefined){
					resolve({
					    message: "USER_EDITED",
					    UserID: userID
					});
					UserModel.find({ _id: userID }, function(error, user) {
					    if (user !== undefined) {
						cache.set(user[0]._id, user[0].toString());
					    }
					});
				    }
				    else reject(new Error("ERROR"));
				}
			    );
			}
			else{
			    reject(new Error("USER_NOT_FOUND"));
			}
		    });
		}
	    });
        }).catch(function(err) {
          throw new Error(err.message);
      });
    },
    editPassword(userID, oldPassword, newPassword){
        return new Promise(function(resolve, reject) {
	    UserModel.find({ _id: userID }, function(error, comments) {
		if (comments !== undefined) {
		    bcrypt
                        .compare(oldPassword, comments[0].password)
                        .then(result => {
			    console.log(result);
                            if (result === true) {
				bcrypt.hash(newPassword, 10, function(err, hash) {
				    UserModel.updateMany(
					{ _id: userID },
					{ $set: { password: hash, updated: new Date(Date.now()).toISOString()} },
					function(error, comments) {
					    if (comments !== undefined){
						resolve({
						    message: "USER_EDITED",
						    UserID: userID
						});
						UserModel.find({ _id: userID }, function(error, user) {
						    if (user !== undefined) {
							cache.set(user[0]._id, user[0].toString());
						    }
						});
					    }
					    else reject(new Error("ERROR"));
					}
				    );
				});
			    }
			    else{
				reject(new Error("OLD_PASSWORD_MISMATCH"));
			    }
			});
		}
		else{
		    reject(new Error("USER_NOT_FOUND"));
		}
	    });
        }).catch(function(err) {
            throw new Error(err.message);
	});
    },
    login(username, password) {
	return new Promise(function(resolve, reject) {
	    UserModel.find({ username }, function(error, comments) {
		if (comments[0] !== undefined) {
		    bcrypt
			.compare(password, comments[0].password)
			.then(result => {
			    if (result === true) {
				jwt.sign(
				    { id: comments[0]._id, username: comments[0].username },
				    process.env.SECRET,
				    { expiresIn: "7d" },
				    function(err, token) {
					resolve({ username, token, id: comments[0]._id });
				    }
				);
			    } else reject(new Error("WRONG_PASSWORD"));
				})
			.catch(function() {
			    reject(new Error("WRONG_PASSWORD"));
			});
		} else reject(new Error("USER_NOT_FOUND"));
	    });
	}).catch(function(err) {
	    throw new Error(err.message);
	});
    },
    getUserFromUsername(username){
	return new Promise(function(resolve, reject){
	    UserModel.find({ username },{"password": 0, "__v": 0, "WebSocketID": 0}, function(error, comments) {
		if (comments[0] === undefined) reject(new Error("USER_NOT_FOUND"));
		else {
		    resolve(comments[0]);
		}
            });	    
	}).catch(function(err) {
	    throw new Error(err.message);
	});
    },
    getUsernameFromID(id) {
	return new Promise(function(resolve, reject) {
	    cache.get(id).then(function(err, value) {
		if (value !== undefined && value !== null) {
          resolve(value);
        } else {
            UserModel.find({ _id: id },{"password": 0, "__v": 0, "WebSocketID": 0}, function(error, comments) {
            if (comments[0] === undefined) reject(new Error("USER_NOT_FOUND"));
            else {
              resolve(comments[0].username);
            }
          });
        }
      });
    }).catch(function(err) {
      throw new Error(err.message);
    });
  },
  delete(username) {
    return new Promise(function(resolve, reject) {
      UserModel.find({ username }, function(error, comments) {
        if (comments[0] === undefined) reject(new Error("USER_NOT_FOUND"));
        else {
          UserModel.deleteOne({ username }, function(error, comments) {
            if (comments !== undefined) resolve("USER_DELETED");
            else reject(new Error(error));
          });
        }
      });
    }).catch(function(err) {
      throw new Error(err.message);
    });
  },
  isUsernameExist(username) {
    return new Promise(function(resolve, reject) {
      // CHECKING CACHE
      cache.get(username, (err, result) => {
        if (result !== undefined && result !== null) {
          resolve(true);
        } else {
          // CACHING
          UserModel.find({ username }, function(error, comments) {
            if (comments !== undefined) {
              cache.set(username, JSON.stringify(comments[0]));
              resolve(true);
            } else {
              resolve(false);
            }
          });
        }
      });
    });
  },
  verifyToken(token) {
      return new Promise(function(resolve, reject) {
	  jwt.verify(token, process.env.SECRET, function(err, decoded) {
              if (decoded !== undefined)
		  resolve(true);
              else
		  reject(false);
      });
    });
  },
  getUserFromToken(token) {
    return new Promise(function(resolve, reject) {
      jwt.verify(token, process.env.SECRET, function(err, decoded) {
        if (decoded !== undefined) resolve(decoded);
        else resolve(false);
      });
    });
  },
  isSocketIDConnected(clientID) {
      return new Promise(function(resolve, reject) {
	  cache.get(clientID, function(err, user){
	      if (user !== null && user !== undefined)
		  resolve(true);
		  else{
		      UserModel.find({ WebSocketID: clientID }, function(error, comments) {
			  if (comments.length !== 0)
			      resolve(true);
			  else
			      resolve(false);
		      });
		  }
	  });
      });
  },
  search(string) {
    return new Promise(function(resolve, reject) {
      UserModel.find(
        { username: { $regex: string } },
        { _id: 1, username: 1 },
        function(error, users) {
          if (users !== undefined) resolve(users);
          else reject("NO_USER");
        }
      );
    });
  },
  getUserFromClientID(WebSocketID) {
      return new Promise(function(resolve, reject) {
	  cache.get(WebSocketID.toString(), function(err, user){
	      if (err !== undefined && err !== null)
		  resolve(JSON.parse(user));
	      else{
		  UserModel.find({ WebSocketID }, {password: 0, __v: 0}, function(error, comments) {
		      if (comments[0] !== null) resolve(comments[0]);
		      else reject(new Error(false));
		  });
	      }
	  });
      }).catch(function(err){
	  throw new Erro(err);
      });;
  },
  connect(userID, clientID) {
      return new Promise(function(resolve, reject) {
	  UserModel.updateMany(
              { _id: userID },
              { $set: { connected: true, WebSocketID: clientID } },
              function(error, comments) {
		  resolve("CONNECTED");
		  UserModel.find({"_id": userID}, function(error, comments){
		      if (comments[0] !== undefined){
			  cache.set(clientID, JSON.stringify(comments[0]));
		      }
		  }).lean();
	      }
	  );
    });
  },
  disconnect(clientID) {
    return new Promise(function(resolve, reject) {
        cache.get(clientID, function(err, user){
	    if (err !== undefined && err !== null){
		user = JSON.parse(user);
		console.log(Date.now() + " " + "[" + clientID + "]" + " " + user.username + " disconnect");
	    }
	});
        cache.del(clientID);
	UserModel.updateMany(
        { WebSocketID: clientID },
        { $set: { connected: false, WebSocketID: null } },
        function(error, comments) {
          resolve("DISCONNECTED");
        }
      );
    });
  },
    auth(token, clientID){
	return new Promise(function(resolve, reject){
	    User.verifyToken(token).then(function(value){
		if (value === true){
		    User.getUserFromToken(token).then(function(user){
			User.connect(user.id, clientID).then(function(value){
			    resolve(value);
			});
		    });
		}
	    }).catch(function(err){
		reject("TOKEN_ERROR");
	    });
	}).catch(function(err) {
	    throw new Error(err);
	});
    },
    getUserFromID(id) {
	return new Promise(function(resolve, reject) {
            UserModel.find({ _id: id }, {"password": 0, "__v": 0, "WebSocketID": 0}, function(error, comments) {
		if (comments[0] !== undefined && comments[0] !== null)
                    resolve(comments[0]);
		else
                    reject("USER_NOT_FOUND");
            });
	});
    },
    uploadPicture(userID, picture) {
        return new Promise(function(resolve, reject) {
            ipfs.add({"content": picture}, function (err, file) {
                if (err) {
                    console.log(Date.now() + " " + err);
                    reject(err);
                }
                console.log(Date.now() + " " + file)
                UserModel.updateMany(
                    { _id: userID },
	            { $set: { picture: file[0].hash } },
                    function(error, comments) {
                        if (comments !== undefined){
                            resolve({
                                message: "PICTURE_UPLOADED",
                                userID: userID
                            });
                            UserModel.find({ _id: userID }, function(error, user) {
                                if (user !== undefined) {
                                    cache.set(user[0].id.toString(), user[0].toString());
                                }
                            });
                        }
                    });
            });
        });
    },
    
};

module.exports = User
