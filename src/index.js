const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

io.of("/").on("connect", socket => {
  console.log(Date.now() + " " + `New Client ${socket.id} connected on WebSocket`);
    require("./socket/event/user.js")(socket);
    require("./socket/event/product.js")(socket);
});

io.listen(1337);
