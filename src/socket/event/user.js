
// SOCKET IO
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

// Import Actions
const User = require("../../actions/user.js");

// Import Misc Actions
const Timer = require("../../actions/timer.js");

const userRegisterEvent = "user:register";
const userEditUsernameEvent = "user:edit:username";
const userEditPasswordEvent = "user:edit:password";
const userUploadPictureEvent = "user:uploadPicture";
const userLoginEvent = "user:login";
const userAuthEvent = "user:auth";
const userDisconnectEvent = "disconnect";
const userGetUserFromUsernameEvent = "user:getUserFromUsername";
const userGetUserFromIDEvent = "user:getUserFromID";
const userGetUsersFromIDsEvent = "user:getUsersFromIDs";

const cache = require("../../db/cache.js").client;

const success = ":success";
const err = ":err";

let start = new Date();

module.exports = function(client) {
    client.on(userRegisterEvent, (username, password) => {
	start = Date.now();
	console.log(Date.now() + " " + userRegisterEvent);
	User.create(username, password)
	    .then(function(user) {
		console.log(Date.now() + " " + Timer.display(start) + userRegisterEvent + " " + username);
		client.emit(userRegisterEvent + success, user);
	    })
	    .catch(function(error) {
                console.log(Date.now() + " " + Timer.display(start) + `${error.message} ${username}`);
		client.emit(userRegisterEvent + err, error.message);
	    });
    });

    client.on(userEditUsernameEvent, function(newUsername) {
	start = Date.now();
	User.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
		User.getUserFromClientID(client.id).then(user => {
		    User.editUsername(user._id, newUsername).then(function(user) {
			client.emit(userEditUsernameEvent + success, user);
			console.log(Date.now() + " " + 
				    `${Timer.display(start)}[${client.id}]` +
				    ` ${userEditUsernameEvent} ${JSON.stringify(user)}`
				   );
			console.log(Date.now() + " " + user);
                    }).catch(function(error){
			console.log(Date.now() + " " + Timer.display(start) + error);
			client.emit(userEditUsernameEvent + err, error.message);
                    });
		})
	    }
	});
    });
    client.on(userEditPasswordEvent, function(oldPassword, newPassword) {
	start = Date.now();
	User.isSocketIDConnected(client.id).then(value => {
            if (value === true) {
		User.getUserFromClientID(client.id).then(user => {
		    User.editPassword(user._id, oldPassword, newPassword).then(function(user) {
			client.emit(userEditPasswordEvent + success, user);
			console.log(Date.now() + " " + 
				    `${Timer.display(start)}[${client.id}]` +
				    ` ${userEditPasswordEvent} ${JSON.stringify(user)}`
				   );
			console.log(Date.now() + " " + user);
                    }).catch(function(error){
			console.log(Date.now() + " " + Timer.display(start) + error);
			client.emit(userEditPasswordEvent + err, error.message);
                    });
		})
	    }
	});
    });

    client.on(userUploadPictureEvent, function(picture){
        start = Date.now();
        console.log(Date.now() + " " + `Receive on ${client.id} user upload picture`);
        User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
                User.getUserFromClientID(client.id).then(function(user) {
                    User.uploadPicture(user._id, picture)
                        .then(function(value) {
                            console.log(Date.now() + " " + Timer.display(start) + "user upload picture " + value);
                            client.emit(userUploadPictureEvent + success, value);
                        })
                        .catch(function(error) {
                            console.log(Date.now() + " " + Timer.display(start) + userUploadPictureEvent + " " + error);
                            client.emit(userUploadPictureEvent + err, error);
                        });
                });
            } else {
                client.emit(userUploadPictureEvent + err, "USER_NOT_CONNECTED");
            }
        });
    });
    
    client.on(userLoginEvent, (username, password) => {
	start = Date.now();
	console.log(Date.now() + " " + userLoginEvent + " " + username);
	User.login(username, password)
	    .then(function(token) {
		console.log(Date.now() + " " + Timer.display(start) + userLoginEvent + success + " " + username);
		client.emit(userLoginEvent + success, token);
	    })
	    .catch(function(error) {
		console.log(Date.now() + " " + Timer.display(start) + `${error.message} ${username}`);
		client.emit(userLoginEvent + err, error.message);
	    });
    });
    
    client.on(userAuthEvent, (token) => {
	start = Date.now();
	console.log(Date.now() + " " + userAuthEvent);
	User.auth(token, client.id)
	    .then(function(user) {
		console.log(Date.now() + " " + Timer.display(start) + "Authenticated");
		client.emit(userAuthEvent + success, user);
	    })
	    .catch(function(error) {
		console.log(Date.now() + " " + Timer.display(start) + "Not Authenticated");
		console.log(Date.now() + " " + error);
		client.emit(userAuthEvent + err, error.message);
	    });
    });

    client.on(userGetUserFromUsernameEvent, function(username) {
        start = Date.now();
        User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
                User.getUserFromUsername(username).then(function(user) {
		    client.emit(userGetUserFromUsernameEvent + success, user);
                    console.log(Date.now() + " " + 
                        `${Timer.display(start)}[${client.id}]` +
                            ` ${userGetUserFromUsernameEvent} ${JSON.stringify(user)}`
                    );
		    console.log(Date.now() + " " + user);
                }).catch(function(error){
		    client.emit(userGetUserFromUsernameEvent + err, error);
		    console.log(Date.now() + " " + username + " " + err);
                });
            }
        });
    });

    
    client.on(userGetUserFromIDEvent, function(userID) {
	start = Date.now();
	User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
		console.log(Date.now() + " " + JSON.stringify(userID));
		User.getUserFromID(userID).then(function(user) {
		    client.emit(userGetUserFromIDEvent + success, user);
		    console.log(Date.now() + " " + 
			`${Timer.display(start)}[${client.id}]` +
			    ` ${userGetUserFromIDEvent} ${JSON.stringify(user)}`
		    );
		    console.log(Date.now() + " " + user);
                }).catch(function(error){
		    client.emit(userGetUserFromIDEvent + err, error);
		    console.log(Date.now() + " " + userID + " " + error);
                });
	    }
	});
    });

    client.on(userGetUsersFromIDsEvent, function(userIDs) {
	start = Date.now();
	User.isSocketIDConnected(client.id).then(async (value) => {
            if (value === true) {
		console.log(Date.now() + " " + JSON.stringify(userIDs));
		var results = [];
		for (i = 0; i < userIDs.length; i++){
		    results.push(await User.getUserFromID(userIDs[i]));
		}
		client.emit(userGetUsersFromIDsEvent + success, results);
		console.log(start - Date.now() + " " + 
			    `${Timer.display(start)}[${client.id}]` +
			    ` ${userGetUsersFromIDsEvent} ${JSON.stringify(results)}`
			   );
		/*}).catch(function(error){
		client.emit(userGetUserFromIDEvent + err, error);
		console.log(Date.now() + " " + userID + " " + error);
            });*/
	    }
	});
    });
    
    client.on(userDisconnectEvent, function(){
	start = Date.now();
	User.disconnect(client.id).then(function(value){
	    console.log(Date.now() + " " + Timer.display(start) + value);
	});
    });
    
};
