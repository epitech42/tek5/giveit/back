// SOCKET IO
const io = require("socket.io")();
const redisAdapter = require("socket.io-redis");

io.adapter(redisAdapter({ host: "redis", port: 6379 }));

// Import Actions
const User = require("../../actions/user.js");
const Product = require("../../actions/product.js");

// Import Misc Actions
const Timer = require("../../actions/timer.js");

const cache = require("../../db/cache.js").client;

const productAddEvent = "product:add";
const productEditEvent = "product:edit";
const productUploadPictureEvent = "product:uploadPicture";
const productListEvent = "product:list";
const productGetInfosEvent = "product:getInfos";
const productDeleteEvent = "product:delete";
const productGetMyProductsEvent = "product:getMyProducts";

const success = ":success";
const err = ":err";

let start = new Date();

module.exports = function(client) {
    client.on(productAddEvent, function(title, content) {
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} new product`);
	User.isSocketIDConnected(client.id).then(value => {
	    console.log(Date.now() + " " + value);
	    if (value === true) {
		User.getUserFromClientID(client.id).then(function(user) {
		    Product.create(title, content, user._id)
			.then(function(value) {
			    console.log(Date.now() + " " + Timer.display(start) + "product added " + value);
			    client.emit(productAddEvent + success, value);
			})
			.catch(function(error) {
			    console.log(Date.now() + " " + Timer.display(start) + productAddEvent + " " + error);
			    client.emit(productAddEvent + err, error);
			});
		});
	    } else {
		client.emit(productAddEvent + err, "USER_NOT_CONNECTED");
	    }
	});
    });
    client.on(productEditEvent, function(productID, title, content){
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} edit product`);
        User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
		User.getUserFromClientID(client.id).then(function(user) {
                    Product.edit(productID, title, content, user._id)
                        .then(function(value) {
			    console.log(Date.now() + " " + Timer.display(start) + "product edited " + value);
                            client.emit(productEditEvent + success, value);
                        })
                        .catch(function(error) {
			    console.log(Date.now() + " " + Timer.display(start) + productEditEvent + " " + error);
                            client.emit(productEditEvent + err, error);
                        });
		});
            } else {
                client.emit(productEditEvent + err, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(productListEvent, function() {
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} list products`);
        User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
                User.getUserFromClientID(client.id).then(function(user) {
                    Product.get()
                        .then(function(value) {
                            console.log(Date.now() + " " + Timer.display(start) + "product listed " + user.username);
                            client.emit(productListEvent + success, value);
                        })
                        .catch(function(error) {
                            console.log(Date.now() + " " + Timer.display(start) + productListEvent + " " + error);
                            client.emit(productListEvent + err, error);
                        });
                });
            } else {
                client.emit(productListEvent + err, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(productGetInfosEvent, function(productID) {
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} get infos products`);
	User.isSocketIDConnected(client.id).then(value => {
	    console.log(Date.now() + " " + value);
	    if (value === true) {
		User.getUserFromClientID(client.id).then(function(user) {
		    Product.getProjectFromID(productID)
			.then(function(value) {
			    console.log(Date.now() + " " + Timer.display(start) + "product infos ${productID}  " + user.username);
			    client.emit(productGetInfosEvent + success, value);
			})
			.catch(function(error) {
			    console.log(Date.now() + " " + Timer.display(start) + productGetInfos + " " + error);
			    client.emit(productGetInfosEvent + err, error);
			});
		});
	    } else {
		client.emit(productGetInfosEvent + err, "USER_NOT_CONNECTED");
	    }
	});
    });
    client.on(productDeleteEvent, function(productID){
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} new product`);
        User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
		User.getUserFromClientID(client.id).then(function(user) {
                    Product.delete(productID, user._id)
                        .then(function(value) {
			    console.log(Date.now() + " " + Timer.display(start) + "product deleted " + value);
                            client.emit(productDeleteEvent + success, {'message': value, 'productID': productID});
                        })
                        .catch(function(error) {
                            console.log(Date.now() + " " + Timer.display(start) + productDeleteEvent + " " + error);
                            client.emit(productDeleteEvent + err, error);
                        });
		});
            } else {
                client.emit(productDeleteEvent + err, "USER_NOT_CONNECTED");
            }
        });

    });
    client.on(productUploadPictureEvent, function(productID, picture){
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} product upload picture`);
        User.isSocketIDConnected(client.id).then(value => {
            console.log(Date.now() + " " + value);
            if (value === true) {
		User.getUserFromClientID(client.id).then(function(user) {
                    Product.uploadPicture(productID, picture, user._id)
                        .then(function(value) {
			    console.log(Date.now() + " " + Timer.display(start) + "product upload picture " + value);
                            client.emit(productUploadPictureEvent + success, value);
                        })
                        .catch(function(error) {
                            console.log(Date.now() + " " + Timer.display(start) + productUploadPictureEvent + " " + error);
                            client.emit(productUploadPictureEvent + err, error);
                        });
		});
            } else {
                client.emit(productUploadPictureEvent + err, "USER_NOT_CONNECTED");
            }
        });
    });
    client.on(productGetMyProductsEvent, function() {
	start = Date.now();
	console.log(Date.now() + " " + `Receive on ${client.id} get infos products`);
	User.isSocketIDConnected(client.id).then(value => {
	    console.log(Date.now() + " " + value);
	    if (value === true) {
		User.getUserFromClientID(client.id).then(function(user) {
		    Product.getProductsFromUserID(user.id)
			.then(function(value) {
			    console.log(value);
			    console.log(Date.now() + " " + Timer.display(start) + "my products ${productID}  " + user.username);
			    client.emit(productGetMyProductsEvent + success, value);
			})
			.catch(function(error) {
			    console.log(Date.now() + " " + Timer.display(start) + productGetInfos + " " + error);
			    client.emit(productGetMyProductsEvent + err, error);
			});
		});
	    } else {
		client.emit(productGetInfosEvent + err, "USER_NOT_CONNECTED");
	    }
	});
    });
};
