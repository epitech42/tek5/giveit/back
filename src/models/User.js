const db = require("../db/db.js");

const User = db.model("User", {
  username: { type: String, required: true },
  password: { type: String, required: true },
  connected: { type: Boolean, required: true, default: false },
  WebSocketID: { type: String, required: true, default: false },
  created: { type: Date, required: true, default: Date.now },
  updated: { type: Date, required: true, default: Date.now },
  picture: { type: String, required: false}  
});
module.exports = User;
