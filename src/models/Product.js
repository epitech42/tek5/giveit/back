const db = require("../db/db.js");

const Product = db.model("Product", {
  title: { type: String, required: true },
  content: { type: String, required: true },
  picture: { type: String, require: false },
  created: { type: Date, required: true, default: Date.now },
  updated: { type: Date, required: true, default: Date.now },
  createdBy: { type: String, required: true },
});
module.exports = Product;
